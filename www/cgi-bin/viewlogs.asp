#!/bin/bash
#================================================
#===  Purpose:	User database managament for app
#===  Created:	MM 2020-07-07
#================================================

PAGE_TITLE='Log '

#================================================
#===  Load our common library
#===---------------------------------------------
COMMON_LIB=0
if [[ -s ${PWD}/common.lib ]]; then . ${PWD}/common.lib; fi
if [[ ${COMMON_LIB} -eq 0 ]]; then
	echo "Content-type: text/html"; echo
	ERR='500 - Server Error - Missing Library'
	echo "<html><head><title>${ERR}</title></head><body>${ERR}</body></html>"
	exit 9
fi

#================================================
#===  Load conf files
#===---------------------------------------------

#=== Load User Conf -----------------------------
if [[ -s ${cAUTOMAT_USER_CONF} ]]; then Load_User_Conf; fi

#=== Log usage ----------------------------------
Log2File 'superadmin' "View: $0"


#================================================
#===  Check for authorized users
#===---------------------------------------------

#=== USERNAME can be expanded in arry, so scrub--
SCRUBBED_USERNAME="$( echo "${REMOTE_USER}" | ${APP_[tr]} -dc "${cSCRUB_STR_USER}" )"
if [[ "${gAUTOMAT_USER_PROFILES[${SCRUBBED_USERNAME}_role]}" != "${cROLES[1]}" ]]; then
	#=== Valid user but not authorized ------
	Log2File 'superadmin' "Access denied: role='${gAUTOMAT_USER_PROFILES[${SCRUBBED_USERNAME}_role]}'"
	Write_Error_Page '401 - Access denied'
fi

#================================================
#===  Parse out requested file
#===---------------------------------------------

#=== Danger Will Robinson! ----------------------
#LOG='../../cgi-bin/viewlogs.cgi'
#=== So: scrub, scrub, scrub! -------------------
LOG="$( echo "${gQUERY_PARAM[log],,}" | ${APP_[tr]} -dc "${cSCRUB_STR_DOMAIN}" )"
LOG="$( echo "${LOG}" | ${APP_[tr]} -dc "${cSCRUB_STR_DOMAIN}" )"
LOG="${LOG//../}"
if [[ -z "${LOG}" || ! -s ${cLOG_DIR}/${LOG} ]]; then LOG="superadmin.$( ${APP_[date]} '+%Y-%m')"; fi


#================================================
#===  Start writing out HTML page
#===---------------------------------------------

echo "Content-type: text/html"; echo
${APP_[sed]} 's/{PAGE_TITLE}/'"${PAGE_TITLE} - ${LOG}"'/' ./cgi_html_template_header

#=== Show Log -----------------------------------
echo "<h3>Showing: ${LOG}</h3>"
echo '<pre>'
${APP_[cat]} ${cLOG_DIR}/${LOG}
echo '</pre>'

#=== Wrap up ------------------------------------
echo "<h4> ${FOOTER_TEXT} </h4>"
echo '</body></html>'

#===[EOF]===
