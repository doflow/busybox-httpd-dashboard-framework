#!/bin/bash
#================================================
#===  Purpose:	Common library for DNS Automat
#===  Created:	MM 2020-07-07
#================================================

#=== Safety settings ----------------------------
unset PATH
#=== Disable pathname expansion (globbing) ------
set -f  #== Or set -o noglob


#=== Note: Busybox trips on IFS=: ---------------
BUSYBOX='/bin/busybox'
declare -A APP_=( \
[awk]='/usr/bin/awk' \
[base64]='/usr/bin/base64' \
[cat]="${BUSYBOX} cat" \
[cp]="${BUSYBOX} cp" \
[date]="${BUSYBOX} date" \
[dd]="${BUSYBOX} dd" \
[dig]='/usr/bin/dig' \
[grep]='/bin/grep' \
[head]="${BUSYBOX} head" \
[kill]="${BUSYBOX} kill" \
[mv]="${BUSYBOX} mv" \
[md5crypt]="${BUSYBOX} httpd -m" \
[sha256]="${BUSYBOX} sha256sum" \
[rm]="${BUSYBOX} rm" \
[sed]="${BUSYBOX} sed" \
[stat]="${BUSYBOX} stat" \
[sort]='/usr/bin/sort' \
[tr]='/usr/bin/tr' \
)

#=== Some handy constants -----------------------
declare -r cSCRUB_STR_USER='_A-Z-a-z-0-9'
declare -r cSCRUB_STR_ROLE='\-A-Z-a-z'
declare -r cSCRUB_STR_DOMAIN=' .\-a-z-0-9'
declare -r cSCRUB_STR_PASSWD='_A-Z-a-z-0-9'
declare -r cSCRUB_STR_QSTRNAME='.-_A-Z-a-z-0-9'
declare -r cSCRUB_STR_BROAD=' .\-_A-Z-a-z-0-9'

#=== Config file locations ----------------------
declare -r cBIND9_KEYS_CONF="${PWD}/keys.conf"
declare -r cPID_FILE="${PWD}/mypid"
declare -r cLOG_DIR="${PWD}/logs"
declare -r cHTTPD_CONF="${PWD}/httpd.conf"
declare -r cAUTOMAT_USER_CONF="${PWD}/dns_automat_user.conf"
declare -r cAUTOMAT_DOMAIN_CONF="${PWD}/dns_automat_domain.conf"
declare -ra cROLES=(Admin Super-Admin Revoke)

#=== Our working global variables ---------------
declare -A gQUERY_PARAM
declare -A gAUTOMAT_USER_PROFILES
gHTTPD_USERS=''
gAUTOMAT_USER_LIST=''
gAUTOMAT_DOMAINS_LIST=''
gDOMAIN_LIST=''


FOOTER_TEXT='The information contained in this electronic forum is intended for the Jasper Wireless'\''s Internal use only.
It may contain privileged and confidential information. If you are not an authorized user of this site, you must not copy,
forward, distribute, use, or take any action in reliance on it. If you have enterted t his website in error,
please notify the webmaster immediately.'

#=== One-line functions -------------------------
UrlDecode() { : "${*//+/ }"; echo -e "${_//%/\\x}"; }
Ensure_RM() { if [[ -f $1 ]]; then ${APP_[rm]} $1; fi; }


#------------------------------------------------
Retrieve_Checkbox_Data() {
#-------#========================================
	#===  Parse out data from checkboxes
	#===-------------------------------------
	local WORD=$1; shift
	echo $* | ${APP_[awk]} 'BEGIN {RS=" "} /^'${WORD}'/ {printf "%s ", $0}' | ${APP_[sed]} 's/'${WORD}'//g'
}

#------------------------------------------------
Write_Select_Options() {
#-------#========================================
	#===  Write our HTML for select option
	#===-------------------------------------
	local ITEM
	for ITEM in $*; do
    		echo "     <option value='${ITEM}'>${ITEM}</option>"
	done
}

#------------------------------------------------
Write_Error_Page() {
#-------#========================================
	#=== Write our HTML for Error Page
	#===-------------------------------------
	local ERR_TXT="$1"

	#=== Basic html page --------------------
	echo "Content-type: text/html"; echo
	echo "<html><head><title>${ERR_TXT}</title></head><body>${ERR_TXT}</body></html>"

	#=== We are done, bail-out --------------
	exit 0
}

#------------------------------------------------
Log2File() {
#-------#========================================
	#=== Write our HTML for Error Page
	#===-------------------------------------
	local TIMESTAMP="$( ${APP_[date]} '+%Y-%m-%d_%H:%M:%S' )"
	local LOGFILE="${cLOG_DIR}/$1.$( ${APP_[date]} '+%Y-%m')"
	local LOGTXT="$2"
	local WWWUSER="${REMOTE_USER:-Unknown}"

	#=== Log columns to file ----------------
	echo "${TIMESTAMP}  ${WWWUSER}  ${LOGTXT}" >> ${LOGFILE}

}

#------------------------------------------------
Password_Suggest() {
#-------#========================================
	#===  Generate a user password
	#===-------------------------------------
	local LEN=16

	#=== Depend on power of SHA -------------
	#${APP_[date]} | ${APP_[sha256]} | ${APP_[base64]} | ${APP_[head]} -c ${LEN}; echo

	#=== Use OS random ----------------------
	#tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | ${APP_[head]} -n1; echo
	< /dev/urandom ${APP_[tr]} -dc "${cSCRUB_STR_PASSWD}" | ${APP_[head]} -c ${LEN}; echo
}

#------------------------------------------------
Update_User_Profile() {
#-------#========================================
	#===  Update user facts in conf
	#===-------------------------------------
	local VERB="$1"
	local USER="$2"
	local ROLE="$3"
	local PASS="$4"
	local DOMAINS="$5"

	local SET_HCONF=9; local SET_UCONF=9

	#=== Scrub username ---------------------
	USER="$( echo "${USER}" | ${APP_[tr]} -dc "${cSCRUB_STR_USER}" )"

	#=== Scrub role -------------------------
	ROLE="$( echo "${ROLE}" | ${APP_[tr]} -dc "${cSCRUB_STR_ROLE}" )"
	if [[ -n "${ROLE}" && "${cROLES[*]/${ROLE}/}" == "${cROLES[*]}" ]]; then ROLE="${cROLES[0]}"; fi

	#=== Log request ------------------------
	Log2File 'superadmin' "${VERB}: '$2' '$3' '${4:+xxx}' '$5'"

	#=== Return if nothing to do ------------
	if [[ -z "${VERB}" || -z "${USER}" ]]; then return; fi
	if [[ "${ROLE}" == "${cROLES[2]}" && "${VERB}" != 'Revoke' ]]; then return; fi


	#=== If locked, return ------------------
	if [[ -s ${cAUTOMAT_USER_CONF}.lck && \
		$(( $( ${APP_[date]} '+%s' ) - $( ${APP_[stat]} -c '%Y' ${cAUTOMAT_USER_CONF}.lck ) )) -lt 60 ]]; then return; fi

	#=== Ensure App conf --------------------
	if [[ ! -s ${cAUTOMAT_USER_CONF} ]]; then
		#=== Generate new file ----------
		echo "admin:${cROLES[1]}:default-password-is.admin remove-me" > ${cAUTOMAT_USER_CONF}
	fi
	#=== Apply lock -------------------------
	${APP_[cp]} ${cAUTOMAT_USER_CONF} ${cAUTOMAT_USER_CONF}.lck
	#=== Backup -----------------------------
	${APP_[cp]} ${cAUTOMAT_USER_CONF} ${cAUTOMAT_USER_CONF}.tmp.$$

	#=== Ensure safe httpd conf -------------
	if [[ ! -s ${cHTTPD_CONF} ]]; then
		#=== Write out new httpd.conf ---
		echo "/cgi-bin:admin:$( ${APP_[md5crypt]} 'admin' )" > ${cHTTPD_CONF}
	fi
	#=== Make working copy ------------------
	${APP_[cp]} ${cHTTPD_CONF} ${cHTTPD_CONF}.tmp.$$

	#=== Only work on backup file -----------
	if [[ ! -s ${cAUTOMAT_USER_CONF}.tmp.$$ || ! -s ${cHTTPD_CONF}.tmp.$$ ]]; then
		Ensure_RM ${cAUTOMAT_USER_CONF}.lck
		Ensure_RM ${cAUTOMAT_USER_CONF}.tmp.$$
		Ensure_RM ${cHTTPD_CONF}.tmp.$$
		return
	fi

	#=== Verb switch ------------------------
	case $1 in
	Add)	#=== Add User ===================
		ROLE=${ROLE:-${cROLES[0]}}
		#=== Check for empty passcode ---
		PASS=${PASS:-$(Password_Suggest)}

		#DOMAINS

		#=== Write out to conf files ----
		#=== Add user httpd.conf---------
set -xv
		if [[ $( ${APP_[grep]} -c "^/cgi-bin:${USER}:" ${cHTTPD_CONF}.tmp.$$ ) -eq 0 ]]; then
			echo "/cgi-bin:${USER}:$( ${APP_[md5crypt]} "${PASS}" )" >> ${cHTTPD_CONF}.tmp.$$
			SET_HCONF=0
		fi

		#=== Add new app conf -----------
		if [[ $( ${APP_[grep]} -c "^${USER}:" ${cAUTOMAT_USER_CONF}.tmp.$$ ) -eq 0 ]]; then
			echo "${USER}:${ROLE}:" >> ${cAUTOMAT_USER_CONF}.tmp.$$
			SET_UCONF=0
		fi
set +xv
		#=== Also add to array-----------
		if [[ ${SET_HCONF} -eq 0 || ${SET_UCONF} -eq 0 ]]; then
			gAUTOMAT_USER_PROFILES[${USER}_role]="${ROLE}"
			gAUTOMAT_USER_PROFILES[${USER}_domains]="${DOMAINS}"
			gAUTOMAT_USER_LIST+="${USER} "
			gHTTPD_USERS+="${USER} "
		fi
		;;

	Revoke)	#=== Delete user ================
set -xv
		${APP_[sed]} -i "/^\/cgi-bin\:${USER}\:/d" ${cHTTPD_CONF}.tmp.$$
		SET_HCONF=$?
		${APP_[sed]} -i "/^${USER}\:/d" ${cAUTOMAT_USER_CONF}.tmp.$$
		SET_UCONF=$?
set +xv
		#=== Also remove user from arry--
		if [[ ${SET_HCONF} -eq 0 && ${SET_UCONF} -eq 0 ]]; then
			unset gAUTOMAT_USER_PROFILES[${USER}_role]
			unset gAUTOMAT_USER_PROFILES[${USER}_profile]
			gAUTOMAT_USER_LIST="${gAUTOMAT_USER_LIST/${USER} /}"
		fi
		;;

	Edit)	#=== Edit user profile ==========
		#=== Fill in the blanks ---------
		ROLE="${ROLE:-${gAUTOMAT_USER_PROFILES[${USER}_role]}}";
		DOMAINS="${DOMAINS:-${gAUTOMAT_USER_PROFILES[${USER}_domains]}}"

		#=== Edit user httpd.conf -------
		if [[ -n "${PASS}"  &&  $( ${APP_[grep]} -c "^/cgi-bin:${USER}:" ${cHTTPD_CONF}.tmp.$$ ) -eq 1 ]]; then
			${APP_[sed]} -i "/^\/cgi-bin:${USER}:/d" ${cHTTPD_CONF}.tmp.$$
			SET_HCONF=$?
			if [[ ${SET_HCONF} -eq 0 ]]; then
				echo "/cgi-bin:${USER}:$( ${APP_[md5crypt]} "${PASS}" )" >> ${cHTTPD_CONF}.tmp.$$
				gHTTPD_USERS+="${USER} ${gHTTPD_USERS/${USER} /}"
			fi
		fi
		#=== Edit user profile ----------
		if [[ $( ${APP_[grep]} -c "^${USER}:" ${cAUTOMAT_USER_CONF}.tmp.$$ ) -eq 1 ]]; then
			${APP_[sed]} -i "/^${USER}:/d" ${cAUTOMAT_USER_CONF}.tmp.$$
			SET_UCONF=$?
			if [[ ${SET_UCONF} -eq 0 ]]; then
				echo "${USER}:${ROLE}:${DOMAINS}" >> ${cAUTOMAT_USER_CONF}.tmp.$$
			fi
		fi

		#=== Also Edit array ------------
		gAUTOMAT_USER_PROFILES[${USER}_role]="${ROLE}"
		gAUTOMAT_USER_PROFILES[${USER}_domains]="${DOMAINS}"
		;;

	esac
#echo "<pre>$(${APP_[cat]} ${cHTTPD_CONF}.tmp.$$)</pre>" >2
#echo "<pre>$(${APP_[cat]} ${cAUTOMAT_USER_CONF}.tmp.$$)</pre>" >2

	#=== Set httpd modification live --------
	if [[ -s ${cHTTPD_CONF}.tmp.$$ && ${SET_HCONF} -eq 0 ]]; then
		#=== Set new conf file ----------
		${APP_[mv]} ${cHTTPD_CONF}.tmp.$$ ${cHTTPD_CONF}

		#=== Notify httpd to refresh ----
		${APP_[kill]} -1 $(< ${cPID_FILE})

	else
		#=== Failed, remove temp file ---
		Ensure_RM ${cHTTPD_CONF}.tmp.$$
	fi

	#=== Set automat conf mods live ---------
	if [[ -s ${cAUTOMAT_USER_CONF}.tmp.$$ && ${SET_UCONF} -eq 0 ]]; then
		#=== Set new conf file ----------
		${APP_[mv]} ${cAUTOMAT_USER_CONF}.tmp.$$ ${cAUTOMAT_USER_CONF}

	else
		#=== Failed, remove temp file ---
		Ensure_RM ${cHTTPD_USER_CONF}.tmp.$$
	fi


	#=== Remove lock ------------------------
	Ensure_RM ${cAUTOMAT_USER_CONF}.lck
}


#------------------------------------------------
Update_Domain_List() {
#-------#========================================
	#===  Update Domain list
	#===-------------------------------------

	#=== Log activity -----------------------
	Log2File 'superadmin' "Domain List mutation: '$*' "

	#=== Scrub list -------------------------
	local DOMAIN_LISTS="$( echo "${1,,}" | ${APP_[tr]} -dc "${cSCRUB_STR_DOMAIN}" )"
	if [[ -z "${DOMAIN_LISTS}" ]]; then return; fi

	#=== Sort list --------------------------
	DOMAIN_LISTS=$( echo ${DOMAIN_LISTS} | ${APP_[tr]} ' ' '\n' | ${APP_[sort]} -bd | ${APP_[tr]} '\n' ' ' )
	DOMAIN_LISTS=${DOMAIN_LISTS//  / }

	#=== Check if worth updating ------------
	if [[ "${DOMAIN_LISTS}" == "${gDOMAIN_LIST}" ]]; then return; fi

	#=== Update -----------------------------
	echo ${DOMAIN_LISTS} | ${APP_[tr]} ' ' '\n' > ${cAUTOMAT_DOMAIN_CONF}
	gDOMAIN_LIST="${DOMAIN_LISTS}"
}


#------------------------------------------------
Load_User_Conf() {
#-------#========================================
	#===  Load User conf from file
	#===-------------------------------------
	local USER_LINE; local _IFS
	local USER; local ROLE; local DOMAINS

	#=== Set our separator ------------------
	_IFS=$IFS; IFS=:
	while read USER_LINE; do
		#=== Quick extra check on empty--
		if [[ -z "${USER_LINE}" ]]; then continue; fi

		#=== Split fields ---------------
		set ${USER_LINE}

		#=== Do some scrubbing ----------
		USER="$( echo "$1" | ${APP_[tr]} -dc "${cSCRUB_STR_USER}" )"
		ROLE="$( echo "$2" | ${APP_[tr]} -dc "${cSCRUB_STR_ROLE}" )"
		DOMAINS="$( echo "${3,,}" | ${APP_[tr]} -dc "${cSCRUB_STR_DOMAIN}" )"

		#=== User Add to array ----------
		gAUTOMAT_USER_PROFILES[${USER}_role]="${ROLE}"
		gAUTOMAT_USER_PROFILES[${USER}_domains]="${DOMAINS}"
		gAUTOMAT_USER_LIST+="${USER} "

	done < <( ${APP_[sort]} ${cAUTOMAT_USER_CONF} )
	#=== Restore IFS ------------------------
	IFS=$_IFS
}


#================================================
#===  If posted data, collect data
#===---------------------------------------------
if [[ "${REQUEST_METHOD}" = "POST" && "${CONTENT_LENGTH}" -lt 2048 ]]; then
	#=== use dd to read POST stdin ----------
	gPOSTED_QUERY_STRING=$( ${APP_[dd]} bs=1 count=${CONTENT_LENGTH} 2>/dev/null )
	if [[ -n "${QUERY_STRING}" ]]; then
		#=== If also append Querystr ----
		QUERY_STRING=${gPOSTED_QUERY_STRING}"&"${QUERY_STRING}
	else
		QUERY_STRING=${gPOSTED_QUERY_STRING}"&"
	fi
fi

#=== Collect query parms ------------------------
while IFS='=' read -r -d '&' KEY VALUE && [[ -n "${KEY}" ]]; do
	#=== Scrub unwanted ---------------------
	KEY="$( echo "${KEY}" | ${APP_[tr]} -dc "${cSCRUB_STR_QSTRNAME}" )"
	VALUE="$( echo "${VALUE}" | ${APP_[tr]} -dc "${cSCRUB_STR_BROAD}" )"

	#=== Add to our QSTR Array --------------
	gQUERY_PARAM["${KEY}"]="${VALUE}"
done <<< $(UrlDecode "${QUERY_STRING}&" )


COMMON_LIB=1
#===[EOF]===
