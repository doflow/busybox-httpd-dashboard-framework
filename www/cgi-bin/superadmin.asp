#!/bin/bash
#================================================
#===  Purpose:	User database managament for app
#===  Created:	MM 2020-07-07
#================================================

PAGE_TITLE='Super-Admin Settings'

#================================================
#===  Load our common library
#===---------------------------------------------
COMMON_LIB=0
if [[ -s ${PWD}/common.lib ]]; then . ${PWD}/common.lib; fi
if [[ ${COMMON_LIB} -eq 0 ]]; then
	echo "Content-type: text/html"; echo
	ERR='500 - Server Error - Missing Library'
	echo "<html><header><title>${ERR}</title><body>${ERR}</body></html>"
	exit 9
fi

#================================================
#===  Load conf files
#===---------------------------------------------

#=== Load httpd users ---------------------------
if [[ -s ${cHTTPD_CONF} ]]; then gHTTPD_USERS="$( ${APP_[awk]} -F':' '/^\/cgi-bin:/ {printf "%s ", $2}' ${cHTTPD_CONF} ) "; fi

#=== Load User Conf -----------------------------
if [[ -s ${cAUTOMAT_USER_CONF} ]]; then Load_User_Conf; fi

#=== Load domains -------------------------------
if [[ -s ${cAUTOMAT_DOMAIN_CONF} ]]; then gDOMAIN_LIST="$( ${APP_[sort]} ${cAUTOMAT_DOMAIN_CONF} | ${APP_[tr]} '\n' ' ' )"; fi


#================================================
#===  Check for authorized users
#===---------------------------------------------

#=== USERNAME can be expanded in arry, so scrub--
SCRUBBED_USERNAME="$( echo "${REMOTE_USER}" | ${APP_[tr]} -dc "${cSCRUB_STR_USER}" )"
if [[ "${gAUTOMAT_USER_PROFILES[${SCRUBBED_USERNAME}_role]}" != "${cROLES[1]}" ]]; then
	#=== Valid user but not authorized ------
	Log2File 'superadmin' "Access denied: role='${gAUTOMAT_USER_PROFILES[${SCRUBBED_USERNAME}_role]}'"
	Write_Error_Page '401 - Access denied'
fi


#================================================
#===  Process Form Request (if any)
#===---------------------------------------------

#echo ${!gQUERY_PARAM[@]} >2
#=== Get ready-for-use selcted domains ----------
SELECTED_DOMAINS="$(Retrieve_Checkbox_Data 'incl_domain_' "${!gQUERY_PARAM[@]}" )"

#=== Need to remove user ------------------------
if [[ "${gQUERY_PARAM[user]}" != '--Add--' && -n "${gQUERY_PARAM[user]}"  && "${gQUERY_PARAM[role]}" == "${cROLES[2]}" ]]; then
	Update_User_Profile 'Revoke' "${gQUERY_PARAM[user]}"

#=== Need to add user ---------------------------
elif [[ "${gQUERY_PARAM[user]}" == '--Add--' && -n "${gQUERY_PARAM[newuser]}" && "${gQUERY_PARAM[role]}" != "${cROLES[2]}" ]]; then
	Update_User_Profile 'Add' "${gQUERY_PARAM[newuser]}" "${gQUERY_PARAM[role]}" "${gQUERY_PARAM[passcode]}"

#=== Need to edit user --------------------------
elif [[ "${gQUERY_PARAM[user]}" != '--Add--' && -n "${gQUERY_PARAM[user]}"  && "${gQUERY_PARAM[role]}" != "${cROLES[2]}" && \
      -n "${gQUERY_PARAM[role]}${gQUERY_PARAM[passcode]}${gQUERY_PARAM[domains]}${SELECTED_DOMAINS}" ]]; then
	Update_User_Profile 'Edit' "${gQUERY_PARAM[user]}" "${gQUERY_PARAM[role]}" "${gQUERY_PARAM[passcode]}" "${SELECTED_DOMAINS}"

#=== Need to update domain-list -----------------
elif [[ "${gQUERY_PARAM[user]}" == '--Add--' && -z "${gQUERY_PARAM[newuser]}" && -z "${gQUERY_PARAM[passcode]}" && \
      -n "${gQUERY_PARAM[domainlist]}" && "${gQUERY_PARAM[domainlist]}" != "${gDOMAIN_LIST}" ]]; then
	Update_Domain_List "${gQUERY_PARAM[domainlist]}"

#=== Log view -----------------------------------
else
	Log2File 'superadmin' "View: $0"

fi

#================================================
#===  Start writing out HTML page
#===---------------------------------------------

echo "Content-type: text/html"; echo
${APP_[sed]} 's/{PAGE_TITLE}/'"${PAGE_TITLE}"'/' ./cgi_html_template_header

#=== For debugging see there we are -------------
#echo "<pre>$( /usr/bin/env )</pre>"
#echo "<h3>QUERY_STRING: '${QUERY_STRING}'</h3>"
#echo "<h3>$( declare -p gQUERY_PARAM )</h3>"
#echo "<pre>$( declare -p gAUTOMAT_USER_PROFILES )</pre>"
#echo "REQUEST_METHOD: ${REQUEST_METHOD} <br>"
#echo "CONTENT_LENGTH: ${CONTENT_LENGTH} <br>"

#=== Write out top menu -------------------------
echo "<table border='1' cellspacing='0' cellpadding='14'>"
echo '<tr><td style="border:none"> <a href='dnsautomat.asp'>DNS Automat</a></td>'
echo '<td style="border:none" colspan="2"><a href='superadmin.asp'>Super-Admin Settings</a></td>'
echo '<td style="text-align:right;border:none;">
<a href='viewlogs.asp?log=dnsautomat.$( ${APP_[date]} '+%Y-%m')'>View Log DNS-Automat</a>&nbsp;
<a href='viewlogs.asp?log=superadmin.$( ${APP_[date]} '+%Y-%m')'>View Log Super-Admin</a>
</td></tr>'

#=== Write out domain list header ---------------
echo "<form action='superadmin.asp' method='post' >"
echo '<tr><th>Domains'
echo "<div id='domainbutton'style='display:none'><br><input type='submit' value='Update'><br></div></th>"
echo "<th colspan='3'><div onclick='showBlock()' style='cursor: pointer;'> <u>${gDOMAIN_LIST}</u> </div>"
echo "<div id='domainedit' style='display:none'><br><textarea name='domainlist' rows='3' cols='70'>
${gDOMAIN_LIST}</textarea></div>"
echo '</th></tr>'
echo "<tr><td colspan='4' style="border:none" ></td></tr>"


#=== Write out user list header -----------------
echo '<tr><th>User</th><th>Role</th><th>Pwd</th><th>Domain List</th></tr>'

#=== write out user table -----------------------
for USER in ${gAUTOMAT_USER_LIST}; do
	#=== User also in sys password db -------
	SYS_SET_USER='***'
	if [[ "${gHTTPD_USERS//${USER} /}" == "${gHTTPD_USERS}" ]]; then
		SYS_SET_USER='No httpd user<superscript>*</superscript>'
		MEMO='<superscript>*</superscript> Set password to add user to system (to allow login)'
	fi

	#=== Write table row ---------------------
	echo "<tr><td> ${USER} </td><td> ${gAUTOMAT_USER_PROFILES[${USER}_role]} </td><td> ${SYS_SET_USER} </td><td> ${gAUTOMAT_USER_PROFILES[${USER}_domains]} </td></tr>"

done

#=== Separator ----------------------------------
echo "<tr><td colspan='4' style="border:none">${MEMO}</td></tr>"
echo "<tr><th colspan='4'>Add/Modify/Delete User Profile (Note: blank fields are ignored)</th></tr>"

#=== Write Updating edit user facts -------------
#echo "<form action='superadmin.asp' method='post' >"
echo "<tr><td><select name='user'>$( Write_Select_Options '--Add--' ${gAUTOMAT_USER_LIST} )</select><br>"
echo "<input type='text' name='newuser' size='5'></td>"
echo "<td><select name='role' size='3'>$( Write_Select_Options ${cROLES[@]} )</select></td>"
echo "<td colspan='2'>Passcode Suggestion: <b>$(Password_Suggest)</b><br>"
echo "<input type='text' name='passcode' size='30' >"
echo "<input type='submit' value='Update'></td>"

echo "<tr><td colspan='4' >"
for ONEDOMAIN in ${gDOMAIN_LIST}; do
	echo "<label><input type='checkbox' name='incl_domain_${ONEDOMAIN}' > ${ONEDOMAIN} </label>&nbsp;"
done
echo '</td></tr>'

echo '</form>'
echo "</table>"

echo "<footer style='position: absolute; bottom: 0; height: 60px;'> ${FOOTER_TEXT} </footer>"
echo '</body>'
echo '</html>'

#===[EOF]===
